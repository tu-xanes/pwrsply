#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## Test macros
#epicsEnvSet("P", "PC")
#epicsEnvSet("N", "1")
#ASYN
epicsEnvSet ("STREAM_PROTOCOL_PATH", "${TOP}/iocBoot/${IOC}")
drvAsynSerialPortConfigure ("PS1","/dev/ttyACM0")
asynSetOption ("PS1", 0, "baud", "9600")
asynSetOption ("PS1", 0, "bits", "8")
asynSetOption ("PS1", 0, "parity", "none")
asynSetOption ("PS1", 0, "stop", "1")
asynSetOption ("PS1", 0, "clocal", "Y")
asynSetOption ("PS1", 0, "crtscts", "N")



## Load record instances
#dbLoadTemplate "db/user.substitutions"
#dbLoadRecords "db/dbSubExample.db", "user=root"
dbLoadRecords("${TOP}/iocBoot/${IOC}/pwrsply.db", "P=$(P), N=$(N)")

## Set this to see messages from mySub
#var mySubDebug 1

## Run this to trace the stages of iocInit
#traceIocInit

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncExample, "user=root"
